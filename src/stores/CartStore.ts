import { defineStore } from 'pinia'
import { type StoreItem } from '../model/product'

export const useCartStore = defineStore('CartStore', {
  state: () => ({
    /** @type {StoreItem[]} */
    items: new Map<string, StoreItem>(),
    visible: false
  }),
  getters: {
    getItems (state): StoreItem[] {
      return Array.from(state.items.values())
    },
    getItemCount (): number {
      return this.getItems.length // map(i => i.qty).reduce((a, b) => a + b)
    },
    isVisible (state): boolean {
      return state.visible
    }
  },
  actions: {
    add (item: StoreItem) {
      const storedItem = this.items.get(item.name)
      if (typeof storedItem === 'undefined') {
        this.items.set(item.name, item)
      } else {
        storedItem.qty += item.qty
      }
    },
    clear () {
      this.items.clear()
    },
    show () {
      this.visible = true
    },
    hide () {
      this.visible = true
    }
  }
})
