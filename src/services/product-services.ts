import data from '../../assets/data.json'
import { Product } from '../model/product'

export function useProductServices (): any {
  const products = new Map<string, Product>()
  data.forEach(p => products.set(p.slug, new Product(p)))

  const fetchProductById = (id: string): Product => {
    return products.get(id) as Product
  }

  const fetchProductsByCategory = (catId: string): Product[] => {
    return [...products.values()].filter(p => p.category === catId)
  }
  return { fetchProductById, fetchProductsByCategory }
}
