import { type StoreItem } from '../model/product'

export const getItemsPrice = (items: StoreItem[]): number => items.map(i => i.price * i.qty).reduce((a, b) => a + b, 0)

export const getShippingPrice = (): number => 50
export const getVatPrice = (itemsPrice: number): number => itemsPrice * 0.2
export const getGrandPrice = (itemsPrice: number): number => itemsPrice + getShippingPrice() + getVatPrice(itemsPrice)
export const getGrandPriceByItems = (items: StoreItem[]): number => getGrandPrice(getItemsPrice(items))
