export function useValidators (): any {
  const isTouched = (val: string): boolean => val !== null
  const isEmpty = (val: string): boolean => val === ''
  const nonEmpty = (val: string, cssClass: string): string => (isTouched(val) && isEmpty(val)) ? cssClass : ''
  const isValidEmail = (val: string): boolean => val !== '' && /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(val)
  const validateEmail = (val: string, cssClass: string): string => (isTouched(val) && !isValidEmail(val)) ? cssClass : ''
  return { nonEmpty, validateEmail }
}
