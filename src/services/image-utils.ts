import { Image, type Product } from '../model/product'
export function useImageUtils (): any {
  const buildImage = (mobile: string, tablet: string, desktop: string): Image => {
    const img: Image = new Image()
    img.mobile = mobile
    img.tablet = tablet
    img.desktop = desktop
    return img
  }
  const getImageAbsPath = (path: string): string => (import.meta.env.VITE_HOST as string) + import.meta.env.BASE_URL + path

  const getImageFullPath = (img: Image): Image => {
    return buildImage(getImageAbsPath(img.mobile), getImageAbsPath(img.tablet), getImageAbsPath(img.desktop))
  }
  const getProductImagesFullPath = (product: Product): Product => {
    const clone = product.clone()
    clone.image = getImageFullPath(product.image)
    clone.categoryImage = getImageFullPath(product.categoryImage)
    clone.gallery.first = getImageFullPath(product.gallery.first)
    clone.gallery.second = getImageFullPath(product.gallery.second)
    clone.gallery.third = getImageFullPath(product.gallery.third)
    clone.others.forEach(other => { other.image = getImageFullPath(other.image) })
    return clone
  }
  return { buildImage, getImageAbsPath, getImageFullPath, getProductImagesFullPath }
}
