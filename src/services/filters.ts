export const currency = (value: number): string => {
  if (isNaN(value)) {
    return '-'
  }
  return '$' + value.toLocaleString()
}
