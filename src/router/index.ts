import { createWebHistory, createRouter } from 'vue-router'
import HomePage from '../pages/HomePage.vue'
import ProductDetailPage from '../pages/ProductDetailPage.vue'
import CheckoutPage from '../pages/CheckoutPage.vue'
import NotFoundPage from '../pages/NotFoundPage.vue'
import CategoryPage from '../pages/CategoryPage.vue'

const routes = [
  {
    path: import.meta.env.BASE_URL,
    redirect: { name: 'home' }
  },
  {
    path: import.meta.env.BASE_URL + 'home',
    name: 'home',
    component: HomePage
  },
  {
    path: import.meta.env.BASE_URL + 'category/:id',
    name: 'category',
    component: CategoryPage,
    props: true
  },
  {
    path: import.meta.env.BASE_URL + 'product/:id',
    name: 'product',
    component: ProductDetailPage,
    props: true
  },
  {
    path: import.meta.env.BASE_URL + 'checkout',
    name: 'checkout',
    component: CheckoutPage,
    props: true
  },
  {
    path: '/:pathMatch(.*)',
    component: NotFoundPage
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior (to, from, savedPosition) {
    // always scroll to top
    return { top: 0 }
  }
})

export default router
