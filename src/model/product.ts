export class Product {
  id: number
  slug: string
  name: string
  image: Image
  category: string
  categoryImage: Image
  new: boolean
  price: number
  description: string
  features: string
  includes: Include[]
  gallery: Gallery
  others: RelatedProduct[]

  constructor (json?: any) {
    this.id = 0
    this.slug = ''
    this.name = ''
    this.image = new Image()
    this.category = ''
    this.categoryImage = new Image()
    this.new = false
    this.price = 0
    this.description = ''
    this.features = ''
    this.includes = []
    this.gallery = new Gallery()
    this.others = []
    if (json != null) {
      this.id = json.id
      this.slug = json.slug
      this.name = json.name
      this.image = new Image(json.image)
      this.category = json.category
      this.categoryImage = new Image(json.categoryImage)
      this.new = json.new
      this.price = json.price
      this.description = json.description
      this.features = json.features
      this.includes = json.includes
      this.gallery = new Gallery(json.gallery)
      this.others = json.others.map((o: any) => new RelatedProduct(o))
    }
  }

  public clone (): Product {
    const p = new Product()
    p.id = this.id
    p.slug = this.slug
    p.name = this.name
    p.image = this.image.clone()
    p.category = this.category
    p.categoryImage = this.categoryImage.clone()
    p.new = this.new
    p.price = this.price
    p.description = this.description
    p.features = this.features
    p.includes = this.includes
    p.gallery = this.gallery.clone()
    p.others = this.others.map(o => o.clone())
    return p
  }
}

export class Image {
  mobile: string
  tablet: string
  desktop: string
  constructor (json?: any) {
    this.mobile = ''
    this.tablet = ''
    this.desktop = ''
    if (json != null) {
      this.mobile = json.mobile
      this.tablet = json.tablet
      this.desktop = json.desktop
    }
  }

  public clone (): Image {
    const i = new Image()
    i.mobile = this.mobile
    i.tablet = this.tablet
    i.desktop = this.desktop
    return i
  }
}

export class Gallery {
  first: Image
  second: Image
  third: Image
  constructor (json?: any) {
    this.first = new Image()
    this.second = new Image()
    this.third = new Image()

    if (json != null) {
      this.first = new Image(json.first)
      this.second = new Image(json.second)
      this.third = new Image(json.third)
    }
  }

  public clone (): Gallery {
    const g = new Gallery()
    g.first = this.first.clone()
    g.second = this.second.clone()
    g.third = this.third.clone()
    return g
  }
}

export class Include {
  quantity: number
  item: string
  constructor () {
    this.quantity = 0
    this.item = ''
  }
}

export class RelatedProduct {
  slug: string
  name: string
  image: Image
  constructor (json?: any) {
    this.slug = ''
    this.name = ''
    this.image = new Image()
    if (json != null) {
      this.slug = json.slug
      this.name = json.name
      this.image = new Image(json.image)
    }
  }

  public clone (): RelatedProduct {
    const rp = new RelatedProduct()
    rp.slug = this.slug
    rp.name = this.name
    rp.image = this.image.clone()
    return rp
  }
}

export interface StoreItem {
  name: string
  image: string
  price: number
  qty: number
}
