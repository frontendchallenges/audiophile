# Frontend Mentor - Interactive rating card component solution

This is a solution to the [Interactive rating card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/audiophile-ecommerce-website-C8cuSd_wx). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Add/Remove products from the cart
- Edit product quantities in the cart
- Fill in all fields in the checkout
- Receive form validations if fields are missed or incorrect during checkout
- See correct checkout totals depending on the products in the cart
  - Shipping always adds $50 to the order
  - VAT is calculated as 20% of the product total, excluding shipping
- See an order confirmation modal after checking out with an order summary
- **Bonus**: Keep track of what's in the cart, even after refreshing the browser (`localStorage` could be used for this if you're not building out a full-stack app)

### Links

- Live Site URL: [Live Solution](https://frontendchallenges.gitlab.io/audiophile/) Unfortunately because there's a private api-key (.env.local file) this is included in .gitignore and this way the solution won't work except locally.
- Screenshot: [Audiophile](./screenshot.png)

## My process

1. Create project `yarn create vite`
   Add dependencies `yarn add --dev sass`
   Edit vite.config.ts to set the base publish URL
   ```base: command === 'build' ? "/audiophile/" : "",```
   Also to add tokens.scss to the global css
2. Create tokens.scss with the constant values for colors, fonts, and sizes, and styles.scss for general styling
  The styles.scss must be included in main.js and tokens.scss in vite.config.ts
3. Set a backgroud color for body
4. Think of the design by splitting the layout in Components:
  App    
    Audiophile (main)
      header
         nav
      view-router
      footer
   Home
      div.left
         h2.subtitle
         h1.title
         p.description
         button.cta-primary
      div.right
         image      
   ProductShop
      link-back.link
      div.left
         image
      div.right
         h2.subtitle
         h1.title
         p.description
         div.price
         div.form-group
            spinner
            button.cta-primary
   Checkout
      link-back.link
      div.left
         h2.title
         form
      div.right
         h2.title
         grid
            img
            productName
            price
            units
            span.total
            button.cta-primary
5. Create a card center vertically and horizontally
6. Think about the desktop design. Use a flex display for the card and when it's in desktop, change flex-direction to row
7. Notice and implement the active states.
8. Implement logic: get the IP Address locations and generate the map.
9. Deploy to gitlab pages (remember configure the base, e.g. vite build --base=./)

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Vue + Vite
- IPify
- Leaflet

### Lessons

How to add a linter:
1. yarn add eslint --dev
2. Add a script in the package.json file "lint": "eslint",
3. yarn lint --init
4. Answer the questions.
5. Set the script in  package.json file "lint": "eslint ./src --fix",
6. Finally to format on Save in vscode, add the following to .vscode/settings.json
```
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },
```

How to set pinia

1. yarn add pinia
2. Create a stores/index.ts file
3. In main.ts, add import {createPinia} from 'pinia', import store from './stores' and createApp().use(store).use(createPinia())


TODO
-Hover/status
-refactor composables?

### Continued development

Implementing this project in Nuxt


### Useful resources

- [How to deploy a Static Site](https://vitejs.dev/guide/static-deploy.html)
- [Created a modal](https://www.digitalocean.com/community/tutorials/vuejs-vue-modal-component)
npx run lerna build

## Author

- Website - [Víctor HG](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments

Always to God and my family.
